<?php
/*
Plugin Name: Unruli Stylists
Plugin URI: http://unruli.com/
Description: Creates the custom functions for Unruli Stylists
Author: Jess Ericskon
Version: 1.0.0
Author URI: http://spirographics.com
License: GPL2
*/

/*** Register Team Category . ***/
function stylist_init() {
    $args = array(
      'label' => 'Team',
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'team'),
        'query_var' => true,
        'menu_icon' => 'dashicons-universal-access-alt',
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'comments',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes',),
            'taxonomies' => array('organization', 'category', 'whatever',)
        );
    register_post_type( 'team', $args );
}
add_action( 'init', 'stylist_init' );

function add_custom_types_to_tax( $query ) {
    if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {

        // Get all your post types
        $post_types = get_post_types();

        $query->set( 'post_type', $post_types );
        return $query;
    }
}