<?php

if (function_exists('curly_business_is_booked_calendar_installed')) {
    if (curly_business_is_booked_calendar_installed()) {
        include_once CURLY_BUSINESS_SHORTCODES_PATH . '/booked-slider/functions.php';
        include_once CURLY_BUSINESS_SHORTCODES_PATH . '/booked-slider/booked-slider.php';
    }
}