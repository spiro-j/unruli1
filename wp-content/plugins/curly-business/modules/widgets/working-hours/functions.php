<?php

if (!function_exists('curly_business_register_working_hours_widget')) {
    /**
     * Function that register sidearea opener widget
     */
    function curly_business_register_working_hours_widget($widgets) {
        $widgets[] = 'CurlyBusinessPhpClassWorkingHours';

        return $widgets;
    }

    add_filter('curly_mkdf_register_widgets', 'curly_business_register_working_hours_widget');
}