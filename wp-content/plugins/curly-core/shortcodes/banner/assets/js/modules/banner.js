(function ($) {
    'use strict';

    var banner = {};
    mkdf.modules.banner = banner;

    banner.mkdfInitBanner = mkdfInitBanner;


    banner.mkdfOnDocumentReady = mkdfOnDocumentReady;

    $(document).ready(mkdfOnDocumentReady);

    /*
     All functions to be called on $(document).ready() should be in this function
     */
    function mkdfOnDocumentReady() {
        mkdfInitBanner();
    }

    /**
     * Banner Shortcode
     */
    function mkdfInitBanner() {
        var bannerHolder = $('.mkdf-banner-holder');

        if (bannerHolder.length) {
            bannerHolder.each(function () {
                $(this).css('height', $(this).width());
            });
        }
    }

})(jQuery);