<?php

if ( ! defined('ABSPATH') ) {
	die('Please do not load this file directly!');
}


function gifeed_instagram_settings_page() {

	ob_start(); ?>
	
	<div class="wrap about-wrap gifeed-class">
		<h1><?php printf( esc_html__( 'Welcome to %s', 'feed-instagram-lite' ), IFLITE_ITEM_NAME ); ?></h1>
		<div class="about-text"><?php printf( esc_html__( 'Thank you for installing this plugin. %s is ready to make your Instagram media more stunning and more elegant!', 'feed-instagram-lite' ), IFLITE_ITEM_NAME ); ?></div>
		<div id="settingpage" class="gifeed-badge">Version <?php echo IFLITE_VERSION; ?></div>
		<hr style="margin-bottom:20px;">
	
		<p id="toscroll" style="font-style:italic; color:rgb(225, 35, 35); border-bottom: 1px dotted #CCC; margin-top: 45px; padding-bottom: 5px;"><span class="dashicons dashicons-megaphone"></span>&nbsp;&nbsp;<?php _e( 'You need an Access Token from Instagram in order to be able to display Instagram media ( images or videos ). Simply click the red button below, log into Instagram and hit Authorize button.', 'feed-instagram-lite' ); ?></p>
	
		<div id="gifeed_config" class="fil-token-list">
			<div class="fil-token-gen-box">
				<span class="fil-token-gen-box-close">&times;</span>
				<input type="text" class="fil-token-gen-username" placeholder="<?php esc_attr_e('Your Instagram Username', 'feed-instagram-lite'); ?>">
				<span data-nonce="<?php echo wp_create_nonce( "fil_instagram_at_nonce"); ?>" class="fil-generate-now"><?php _e('Generate', 'feed-instagram-lite'); ?></span>
				</div>
			<span class="gifeed_generate_token_button"><?php _e('Generate Access Token', 'feed-instagram-lite'); ?></span>
            <hr style="margin-top:20px;">
            <div class="fil-each-token-cont">
            <span class="fil_token_tbl_usr"><?php _e('Username', 'feed-instagram-lite'); ?></span><span class="fil_token_tbl_token"><?php _e('Access Token', 'feed-instagram-lite'); ?></span><span class="fil_token_tbl_action"><?php _e('Action', 'feed-instagram-lite'); ?></span>
            <div class="fil-each-token-list">
			 <?php
			 
			 	$all_token = gifeed_opt( 'access_token' );
				
				if ( ! empty( $all_token ) ) {
					
					foreach ( $all_token as $key => $val ) {
						
						echo '<div data-token-id="'.esc_attr( $key ).'" class="fil_each_token"><span class="fil_token_dtl fil_token_usr">'.esc_html( $val['id'] ).'</span><span class="fil_token_dtl fil_token_token">'.esc_html( $val['access_token'] ).'</span><span class="fil_token_dtl fil_token_delete dashicons dashicons-trash"></span></div>';
						
					}
					
				}
			 
			 ?>
             	<span class="fil_no_token"><?php _e( 'No Access Token', 'feed-instagram-lite' ); ?></span>
              </div>
			 </div>
		 </div>
	
		<div class="feature-section fil-sections">
			<h1 id="gfonts_section" style="font-size:26px;margin-bottom:20px;"><span class="dashicons dashicons-admin-generic" style="margin: 7px 10px 0px 0px;"></span><?php _e( 'General Settings', 'feed-instagram-lite' );?><span class="update_notify"></span></h1>
			<p class="faq-question"><span class="dashicons dashicons-admin-tools" style="margin-right: 5px;margin-top: 2px;"></span><?php _e( 'Plugin Auto Update', 'feed-instagram-lite' );?></p>
			<p class="opt-desc"><?php _e( 'We recommend you to enable this option to get the latest features and other important updates of this plugin.', 'feed-instagram-lite' );?></p>
			<div class="gifeed-opt-cont">
			<?php $gifeed_opt_updt = gifeed_opt( 'gif_instagram_opt_autoupdate' ); ?>
				<input type="radio" data-nonce="<?php echo wp_create_nonce( "gif_instagram_opt_autoupdate"); ?>" data-opt="gif_instagram_opt_autoupdate" name="gif_instagram_opt_autoupdate" <?php echo $gifeed_opt_updt == "active" ? "checked=\"checked\"" : "";?> value="active"><label style="vertical-align: baseline;"><?php _e( "Enable", 'feed-instagram-lite' ); ?></label>
				<input type="radio" data-nonce="<?php echo wp_create_nonce( "gif_instagram_opt_autoupdate"); ?>" data-opt="gif_instagram_opt_autoupdate" name="gif_instagram_opt_autoupdate" <?php echo $gifeed_opt_updt == "inactive" ? "checked=\"checked\"" : "";?> style="margin-left: 10px;" value="inactive"><label style="vertical-align: baseline;"><?php _e( "Disable", 'feed-instagram-lite' ); ?></label>
				<span class="update_notify"></span>
			</div>
		</div>
		
		
	</div>
	<!-- Content End -->
		<?php
		echo ob_get_clean();
	
}