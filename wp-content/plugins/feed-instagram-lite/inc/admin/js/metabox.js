jQuery(document).ready(function($) {

	var links = jQuery('.gifeedtabcon li a');
	var tabcont = jQuery("#tabs_container");
	var refreshTime;
	var validationClue = false;
	
	setTimeout(function(){
		
		$(".gifeeddefaulttab").trigger("click");
		
		gifeed_feedbuilder_id_filter();
  
  	}, 300);
	
	
	// Show reminder to validate
	$('#publish').on('click', function () {
		
		if ( validationClue == true ) {
			
			$("#insta-username-validation").notify("Please Validate first before Save / Update your Feed",{
				position:"top-right",
				arrowSize: 10,
				});
			
			return false;
		}
		
	});
			
    $("#gifeed_feedbuilder_format_users").focusout(function(){
		
		validationClue = true;
		
	});
			
			
	$('.gifeedtabcon li a').on('click', function () {

		if ($(this).attr('id') == 'adv' ) {
			
			refreshTime = setTimeout(refreshCodeMirror, 1000);
			
		}
				
		$(tabcont).hide();
		$(".tabloader").css("height", "300").addClass("tbloader");
		$(tabcont).find("tr").hide();
	
		$(tabcont).find("."+jQuery(this).attr("id")+"").fadeIn(500, function() {
			$(tabcont).fadeIn("slow");
			$(".tabloader").css("height", "auto").removeClass("tbloader");
		});				
				
     	links.removeClass('tabulous_active');
	 	$(this).addClass('tabulous_active');
								
	});
	
	function refreshCodeMirror() {
		
		$('.CodeMirror').each(function(i, el){
			el.CodeMirror.refresh();
			});
			
			clearInterval(refreshTime);
			
	}
	
	//	Feed Builder Field
	function gifeed_feedbuilder_id_filter() {
		
		var $iselmt = $('#gifeed_feedbuilder_format_users');

		if ($iselmt.val() == '') {
			$('#insta-username-validation').attr('disabled','disabled').css('pointer-events', 'none');
			} else {
				$('#insta-username-validation').removeAttr('disabled').css('pointer-events', '');
				}
		
		// Remove last comma
		if ($iselmt.val().charAt(0) == ',') {
			$iselmt.val($iselmt.val().substring(1));
			}
		
		if ($iselmt.val().indexOf(',') !== -1) {
			$('.feed_bulk_options').fadeIn(500);
			
			} else {
				$('.feed_bulk_options').fadeOut(100);
				$(".feed_bulk_options #individual").prop("checked", true);
				}
		
		// Remove double comma		
		if ($iselmt.val().indexOf(',,') !== -1) {
			$iselmt.val($iselmt.val().replace(/,,/g, ','));
			}
			
		// Only allow alphabet, number, underscore and #
		if ($iselmt.val().match(/[^a-zA-Z0-9,#_. ]/g)) {
			$iselmt.val($iselmt.val().replace(/[^a-zA-Z0-9,#_. ]/g, ''));
            }
				
	} // End gifeed_feedbuilder_id_filter
	

	// Validate Username	
	jQuery('#insta-username-validation').click(function(){
		
		var all_element = new Array();
		
		all_element = [$(this), $('#gifeed_feedbuilder_format_users'), $('.feed_validation_info'), $('#validation_status'), $('.btn_loading'), $('.items_to_val')];
		
		gifeed_validate_usernames(all_element);
		
	});		
		
		
	// Validate Username
	function gifeed_validate_usernames(elmt) {

		// Skip hastag(s) when validate username
		var res = new Array();
		str = elmt[1].val().replace(/,\s*$/, "");
			res = str.split(",");
			
			
		if ( ! res.length ) {
			
				elmt[2].fadeIn(300);
				elmt[3].removeClass().addClass('button_loading');
				elmt[4].css('display','inline-block');
				elmt[5].html('Please wait...');
				elmt[0].attr('disabled','disabled');
				elmt[1].attr('disabled','disabled');
				
			setTimeout(
			function() {
				elmt[3].removeClass().addClass('validation_valid');
				elmt[5].html('Done...');
			}, 500);		
			
			setTimeout(
			function() {
				$(elmt[2]).fadeOut(300);
				elmt[4].hide();
				elmt[0].removeAttr('disabled');
				elmt[1].removeAttr('disabled');
			}, 1500);					
								
		}
			
			
		// Check valid user ( one by one ) begin!	
		$.each( res, function( index, value ){
			
			var querySearch, inCheck, isID, isName, isHash;
			
			setTimeout(function () {
			
				elmt[2].fadeIn(300);
				elmt[3].removeClass().addClass('button_loading');
				elmt[4].css('display','inline-block');
				elmt[5].html('Please wait...');
				elmt[0].attr('disabled','disabled');
				elmt[1].attr('disabled','disabled');
				
				// Check for hashtags or Username
				if ( value.indexOf('#') > -1) {
					
					var cleanHashtag = value.replace('#', '');
					querySearch = 'https://api.instagram.com/v1/tags/search?q='+cleanHashtag+'&access_token='+gifeed_metabox_opt.token+'';
					inCheck = 'hashtags';
					
				} else {
					
					querySearch = 'https://api.instagram.com/v1/users/self/?access_token='+value+'';
					inCheck = 'token';
					
				}
			
				$.ajax({
					type: "GET",
					dataType: "jsonp",
					cache: false,
					url: encodeURI(querySearch),
					success: function(data) {
												
						if ( $.isEmptyObject(data.data) ) {
					
							elmt[1].val(elmt[1].val().replace(value, ''));
							gifeed_feedbuilder_id_filter();
							elmt[3].removeClass().addClass('validation_invalid');	
							elmt[5].html('<span style="color:red !important;"><strong>'+value+'</strong> '+inCheck+' is not valid!</span>');
						
							return;
						
						} else {
						
							elmt[3].removeClass().addClass('validation_valid');
							
							if (inCheck == 'token') {
								
								isID = value;
								isName = value;
								isHash = '';
								
							} else {
								
								isID = data.data[0].name;
								isName = data.data[0].name;
								isHash = '#';
								
							}

							elmt[5].html('<span style="color:blue !important;"><strong>'+isName+'</strong> '+inCheck+' is valid</span>');
							elmt[1].val(elmt[1].val().replace(value, isHash+isName));
						}
					}
							
				}); // End Ajax
			
			    	if (index+1 == res.length) {
						setTimeout(
							function() {
								$(elmt[2]).fadeOut(300);
								elmt[4].hide();
								elmt[1].val(elmt[1].val().replace(/,\s*$/, ""));
								elmt[0].removeAttr('disabled');
								elmt[1].removeAttr('disabled');
								gifeed_feedbuilder_id_filter();
								validationClue = false;
								
								}, 1500);
							}

				}, index*1000);
			
		}); // End Each

	} // End gifeed_validate_usernames
	
	
	// Validate username(s)
	$(document).on("keyup contextmenu input", '#gifeed_feedbuilder_format_users', function () {
		
		gifeed_feedbuilder_id_filter();
			
		});
		
		
	// Preview
	$("#gifeed-preview").click(function(){
		
		$("#post").attr("target","_blank");
		$("#post").attr("action","admin-ajax.php");
		$("#hiddenaction").val("gifeed_generate_preview");
		$("#originalaction").val("gifeed_generate_preview");
		$("<input>").attr("type","hidden").attr("name","action").attr("id","gifeed_preview").val("gifeed_generate_preview").appendTo("#post");
		$("#post").submit();
		$("#post").attr("target","");
		$("#post").attr("action","post.php");
		$("#hiddenaction").val("editpost");
		$("#gifeed_preview").remove();
		$("#originalaction").val("editpost");
		
		if ( validationClue == true ) {
			$('#publish').removeClass('disabled');
			$('.spinner').removeClass('is-active');
			}

		});
		

	// Typography
	var fontloader;
	
	$(".gifeed_font_list option[value=google-font]").attr('disabled','disabled').addClass('gifeed_list_disabled');
	$(".gifeed_font_list option[value=default-font]").attr('disabled','disabled').addClass('gifeed_list_disabled');
	
	
	/* Font Preview */
	jQuery(".gifeed_font_preview").click(function(){
		alert('Pro version only!');
	
	});

	
	/* Reset Typography */
	jQuery(".gifeed_font_reset").click(function(){
		
		jQuery('.gifeed-typo').each(function(){
			
			var value = jQuery(this).attr('data-def');
			
			jQuery(this).attr('value', value);
			if ( jQuery(this).hasClass('backonly') ) jQuery(this).prev().find('.col-indicator').css('background-color', value);
			
		});
		
	});
	

	/* Select Access Token */
	jQuery('.fil_available_token').on('change', function() {
		
		var defV = jQuery('#gifeed_feedbuilder_format_users').val();
		
		if (this.value == 'none' || defV.indexOf(this.value) > -1 ) return false;
		
		jQuery('#gifeed_feedbuilder_format_users').val(defV+','+this.value);
		
		setTimeout(function(){
			
			gifeed_feedbuilder_id_filter();
	  
		}, 100);
		
	});


	$("#tabs_container *").attr("disabled", "disabled").off('click');
	
		var over = '<div class="overlay">' +
            '</div><a href="'+gifeed_metabox_opt.upgrade_link+'"><div class="upgradenow hvr-wobble-bottom"></div></a>';
        $(over).appendTo('#tabs_container');	
	
	// Video Tutorial
	MediaBox('.mediabox');
	
	
}); // End Doc Ready

/*! mediabox v0.0.2 | (c) 2016 Pedro Rogerio | https://github.com/pinceladasdaweb/mediabox */
(function (root, factory) {
    "use strict";
    if (typeof define === 'function' && define.amd) {
        define([], factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        root.MediaBox = factory();
    }
}(this, function () {
    "use strict";

    var MediaBox = function (element) {
        if (!this || !(this instanceof MediaBox)) {
            return new MediaBox(element);
        }

        this.selector = document.querySelectorAll(element);
        this.root     = document.querySelector('body');
        this.run();
    };

    MediaBox.prototype = {
        run: function () {
            Array.prototype.forEach.call(this.selector, function (el) {
                el.addEventListener('click', function (e) {
                    e.preventDefault();

                    var link = this.parseUrl(el.getAttribute('href'));
                    this.render(link);
                    this.close();
                }.bind(this), false);
            }.bind(this));
        },
        template: function (s, d) {
            var p;

            for (p in d) {
                if (d.hasOwnProperty(p)) {
                    s = s.replace(new RegExp('{' + p + '}', 'g'), d[p]);
                }
            }
            return s;
        },
        parseUrl: function (url) {
            var service = {},
                matches;

            if (matches = url.match(/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/)) {
                service.provider = "youtube";
                service.id       = matches[2];
            } else if (matches = url.match(/https?:\/\/(?:www\.)?vimeo.com\/(?:channels\/|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/)) {
                service.provider = "vimeo";
                service.id       = matches[3];
            } else {
                service.provider = "Unknown";
                service.id       = '';
            }

            return service;
        },
        render: function (service) {
            var embedLink,
                lightbox;

            if (service.provider === 'youtube') {
                embedLink = 'https://www.youtube.com/embed/' + service.id;
            } else if (service.provider === 'vimeo') {
                embedLink = 'https://player.vimeo.com/video/' + service.id;
            } else {
                throw new Error("Invalid video URL");
            }

            lightbox = this.template(
                '<div class="mediabox-wrap"><div class="mediabox-content"><span class="mediabox-close"></span><iframe src="{embed}?autoplay=1" frameborder="0" allowfullscreen></iframe></div></div>', {
                    embed: embedLink
                });

            this.root.insertAdjacentHTML('beforeend', lightbox);
        },
        close: function () {
            var wrapper = document.querySelector('.mediabox-wrap');

            wrapper.addEventListener('click', function (e) {
                if (e.target && e.target.nodeName === 'SPAN' && e.target.className === 'mediabox-close') {
                    wrapper.classList.add('mediabox-hide');
                    setTimeout(function() {
                        this.root.removeChild(wrapper);
                    }.bind(this), 500);
                }
            }.bind(this), false);
        }
    };

    return MediaBox;
	
}));