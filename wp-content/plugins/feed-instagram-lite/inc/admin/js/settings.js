jQuery(document).ready(function($) {
	
	$('.update_notify').hide();

	if(window.location.hash) {
		
		var realHash, hash;
		
		hash = window.location.hash.split('=')[1];
		
		realHash =  hash.split('.')[0];
		
		switch ( realHash ){
			
			case 'e400':
			case 'settingpage':
			
			$(".gifeed_generate_token_button").notify("Please Generate your Instagram Access Token first.",{
				position:"right-middle",
				arrowSize: 5,
			});
			
			break;
			
	  		default:
			
				try {
					
					var ig_data = fil_base64_decode(realHash);
				
					ig_data = JSON.parse(ig_data);
					
					if (ig_data.hasOwnProperty('access_token') && ig_data.access_token != '') {
						
						var id = ig_data.id,
						access_token = ig_data.access_token;
						
						var data = {
							action: 'gifeed_ajax_access_token',
							task: 'add',
							security: $('.fil-generate-now').data('nonce'),
							token_data: ig_data
						};
						
						$.post(ajaxurl, data, function(response) {
							
							response = JSON.parse(response);
							
							if (response.hasOwnProperty('error') ) {
								
								$('.gifeed-class').append('<div id="dialog-confirm" title="Instagram Info"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>'+response.error+'</p></div>');
								
								$( "#dialog-confirm" ).dialog({
									resizable: false,
									height: "auto",
									width: 400,
									modal: true,
									dialogClass: 'no-close',
									closeOnEscape: false,
									open: function(event, ui) {
										$(event.target).dialog('widget')
											.css({ position: 'fixed' })
            								.position({ my: 'center', at: 'center', of: window });
									},
									buttons: {
									  "Logout Now": function() {
										  window.open('https://www.instagram.com/accounts/logout/', '_blank');
										  $('.gifeed_generate_token_button').trigger('click');
										  $( this ).dialog( "close" );
									  },
									  Cancel: function() {
									  $('.gifeed_generate_token_button').trigger('click');
										$( this ).dialog( "close" );
									  }
									}
								});
							
								return false;
							
							}
							
							if (response.hasOwnProperty('ok') ) {
								
								$('.fil_no_token').hide();
								$('.fil-each-token-list').append($(response.ok));
								
								setTimeout(function(){
									$('html, body').animate({scrollTop: $('[data-token-id="'+access_token+'"]').offset().top}, 1000);
									$('[data-token-id="'+access_token+'"]').effect( "highlight", {color:"#f0d377"}, 3000 );
								}, 500);
								
							}
							
							else {
								alert('Ajax request failed, please refresh your browser window.');
							}
						
						});
						
					}
				
				}
				catch(e){alert('Error!');}
		
		}

	}
	
	// Validate username(s)
	$(document).on("keyup contextmenu input", '#gifeed_google_fonts_api_key', function () {
		
		$(this).val($.trim($(this).val()));
			
		});
		
	/* General Settings */
	$('.gifeed-opt-cont input').not('#gifeed_google_fonts_api_key').bind('click', function() {
		
		loadingOverlay();
		gifeed_ajax_options($(this));
		
	});
	
	function gifeed_ajax_options(el) {

		var data = {
			action: 'gifeed_ajax_update_settings',
			security: $(el).attr('data-nonce'),				
			cmd: [$(el).attr('data-opt'), $(el).val()],
			};
			
			$.post(ajaxurl, data, function(response) {
	
				var notify = $(el).parent().find('.update_notify');
				notify.hide();
				notify.removeClass('notifyupdated notifyerror');
				
				if (response == 1) {
					$('#overlay').remove();
					notify.removeClass('notifyupdated notifyerror').addClass('notifyupdated').fadeIn(500, function() {
						notify.fadeOut(2000);
					});
					
					}						
					else {
						$('#overlay').remove();
						notify.removeClass('notifyupdated notifyerror').addClass('notifyerror').fadeIn(500, function() {
						notify.fadeOut(2000);
						});
						alert('Ajax request failed, please refresh your browser window.');
						}
						
			});
			
	}
	
		
	function afterAjax(el, msg, sts) {
		
		$(el[0]).removeAttr('disabled');
		$('#overlay').remove();
		el[1].text(msg).css('color', sts).fadeIn(2000, function() {
			el[1].fadeOut(2000);
			});

	}
	
	
	function loadingOverlay() {
		
		var over = '<div id="overlay">' +
            '<div id="loading"></div>' +
            '</div>';
        $(over).appendTo('#wpcontent');	
		
	}
	
	function fil_base64_encode(input) {
		
		if( input === undefined || input === '' ){
			return '';
		}
		
		try
		{
			return window.btoa(unescape(encodeURIComponent( input )));
		}
		catch (ex){return input;}
		
	}	
	
	function fil_base64_decode(input) {
		
		if( input === undefined || input === '' ){
			return '';
		}
		
		try
		{
			return decodeURIComponent(escape(window.atob( input )));
		}
		catch (ex){return input;}
		
	}	
	
	
	// Token Generator
	$('.gifeed-class').on('click', '.gifeed_generate_token_button', function() {
		$('.fil-token-gen-box').fadeIn(300, function(){
			$('.fil-token-gen-username').focus();
		});
	});
	
	$('.gifeed-class').on('click', '.fil-token-gen-box-close', function() {
		$('.fil-token-gen-box').fadeOut(100, function(){
			$('.fil-token-gen-username').val('');
		});
	});
	
	$('.gifeed-class').on('click', '.fil-generate-now', function() {
		
		if ($('.fil-token-gen-username').val() == '') {
			alert('Please fill your Instagram username first');
			$('.fil-token-gen-username').focus();
			return false;	
		}
		
		$(this).text('Please wait...');
		var oauth_url = gifeed_admin_settings_script_opt.token_gen_link,
		redirect_url = gifeed_admin_settings_script_opt.redirect_uri,
		ig_uname = '&ig_username='+$('.fil-token-gen-username').val();
		window.location = oauth_url+'&redirect_uri=https://instagram.ghozylab.com/?return_uri='+fil_base64_encode(redirect_url+ig_uname)+'&response_type=token';
	});
	
	$('.gifeed-class').on('click', '.fil_token_delete', function(e) {
		
		var result = confirm('Are you sure?');
		
		if (result) {
			
			var token_id = $(this).closest('.fil_each_token').data('token-id'),
			data = {
				action: 'gifeed_ajax_access_token',
				task: 'remove',
				security: $('.fil-generate-now').data('nonce'),
				token_id: token_id
			};
			
			$.post(ajaxurl, data, function(response) {

				if (response == 'deleted') {
					
					$('[data-token-id="'+token_id+'"]').fadeOut(100, function(){
						this.remove();
						fil_recount_token();
					});
					
				}
				
				else {
					alert('Ajax request failed, please refresh your browser window.');
				}
			
			});
			
		}
		
		e.preventDefault();
	});
	
	function fil_recount_token() {
		
		if ($('.gifeed-class').length) {
			setTimeout(function(){
				if ($('.fil_each_token').length == 0) $('.fil_no_token').css('display', 'block');
			}, 500);
		}
					
	}
	
	fil_recount_token();
	
});