=== Instagram Feed Gallery ===
Contributors: GhozyLab
Tags: instagram, instagram gallery, instagram feed, gallery, instagram photo, instagram plugin, widget, images, gallery instagram, image, responsive gallery
Requires at least: 3.3
Tested up to: 5.0
Stable tag: 1.0.0.5
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Instagram Feed Plugin will help you to display your Instagram photo as gallery based on username or hashtag easily.

== Description ==

https://www.youtube.com/watch?v=PN80JilwP3Y

[Homepage](https://demo.ghozylab.com/plugins/instagram-feed-plugin/) | [Documentation](https://www.youtube.com/watch?v=PN80JilwP3Y) | [Support](https://demo.ghozylab.com/plugins/instagram-feed-plugin/#!/getintouch) | [Demo](https://demo.ghozylab.com/plugins/instagram-feed-plugin/#!/demo) | [Premium Version](https://demo.ghozylab.com/plugins/instagram-feed-plugin/)

= Instagram Plugin =
Are you looking for [Instagram Feed or Instagram Gallery Plugin](https://wordpress.org/plugins/feed-instagram-lite/) and want to integrate Instagram photo and video in website? This Instagram plugin will help you to make beautiful gallery of your Instagram photos with masonry layout in your WP site just in minutes.

You can fetch images or video specifically from your account and other user accounts or even only from hashtags. This plugin will automatically fill your site with linked thumbnails for your Instagram pictures. You can easily change this behavior using the standard options provided. The only thing you will need to get going is a valid Instagram token ID and you can generate the token from settings page with just one click.

This plugin also provide the Shortcode Generator, so once again you can easily display or embed your Instagram Feed anywhere you want. Very easy to use and zero coding needed. Plus, you will have a pretty fancy-looking [Instagram Feed](https://wordpress.org/plugins/feed-instagram-lite/) Gallery in your lovely sites.

= Key Features =

* **Unlimited Instagram Usernames or Hashtags** : You can display photos or videos from other Instagram accounts, just use separate multiple username using commas. You can also combine usernames and hashtags and also separated by commas. 
* **Amazing Feeds Builder** : Made with usability in mind, with a ton of options anyone can create amazing Instagram gallery in just a few minutes.
* **WordPress Shortcode Generator** : We provide dedicated Shortcode Generator that give you ease to add some really cool functionality from this plugin into your site just in one click!
* **Visual Composer Support** : Install and manage Instagram Feed Lite as Visual Composer Element. Instagram Feed Lite supports one of the most popular page builders natively.
* **100% Responsive** : This Instagram plugin is 100% responsive and compatible with mobile, tablets, desktop computers and all modern web browsers which include iPhone, iPad, Android, Chrome, Safari, Firefox, Opera, Internet Explorer 7/8/9/10/11 and also Microsoft Edge.

= Upgrade to Pro Version = 

> #### **Pro Version Demo**
> * [Elegant Masonry](https://demo.ghozylab.com/plugins/instagram-feed-plugin/#!/eleganmasonry "Elegant Masonry")
> * [Custom Skin ( Blue )](https://demo.ghozylab.com/plugins/instagram-feed-plugin/#!/simpleblue "Simple Blue")
> * [Classic 3 Columns](https://demo.ghozylab.com/plugins/instagram-feed-plugin/#!/classic3col "Classic 3 Columns")
> * [Thumbnails](https://demo.ghozylab.com/plugins/instagram-feed-plugin/#!/thumbnails "Thumbnails")

Take your Instagram Plugin to the next level with [Instagram Feed Pro](https://demo.ghozylab.com/plugins/instagram-feed-plugin/ "best instagram plugin"), which gives you additional features such as:

* 100% Responsive
* Unlimited colors and layout
* +700 Fonts from Google Fonts
* +65 Options
* Open image / video in Lightbox
* Support Instagram Comments
* SEO-friendly and SEO Images
* Custom CSS and JS
* Custom Template
* Advanced Typography
* Manage Title, followers, Likes & Comments
* and much more...

### Recommended Plugins
The following are other recommended plugins by the author:

* [Gallery Plugin](https://wordpress.org/plugins/easy-media-gallery/ "gallery plugin") - Easy Media Gallery is a wordpress plugin designed to display various media support including grid gallery, galleries, photo album, multiple photo albums, uteam, photo gallery or image gallery.
* [Slider Plugin](https://wordpress.org/plugins/image-slider-widget/ "slider plugin") - Easy Image Slider Widget - Displaying your image as slider in widget/sidebar area with very easy. Allows you to customize it to looking exactly what you want.
* [Form Plugin](https://wordpress.org/plugins/contact-form-lite/ "Contact Form") - The Best Contact Form Plugin to create awesome Contact Form in minutes.
* [Web Icon Fonts Plugin](https://wordpress.org/plugins/icon/ "Icon Fonts Plugin") - Select and insert icon into your post or page just in one click. More than 2.5k icons available.
* [Best Popup Plugin](https://wordpress.org/plugins/easy-notify-lite/ "optin form plugin") -  The Best Notify and Subscription Form Plugin to display notify popup, announcement and subscribe form with very ease, fancy and elegant.
* [Best Image Carousel](https://wordpress.org/plugins/image-carousel/ "image carousel") - Touch enabled WordPress plugin that lets you create a beautiful responsive image carousel.
* [Best Gallery Lightbox](https://wordpress.org/plugins/gallery-lightbox-slider/ "Gallery Lightbox") - Gallery Lightbox - Displays all gallery images into the lightbox slider in just a few seconds.

> #### **NOTE**
> If you would like to create your own language pack or update the existing one, you can send <a href="https://codex.wordpress.org/Translating_WordPress" title="Translating WordPress" target="_blank">the text of PO and MO files</a> for <a href="https://ghozylab.com/plugins/" title="GhozyLab" target="_blank">GhozyLab</a> and we'll add it to the plugin. You can download the latest version of the program for work with PO and MO files  <a href="https://poedit.net/download" title="Download Poedit" target="_blank">Poedit</a>.

= Technical Support =

If any problem occurs or if you think, that you found a bug please contact us at [here](https://redirect.ghozylab.com/mailto.php "Contact Us").

== Installation ==

= For automatic installation: =

The simplest way to install is to click on 'Plugins' then 'Add' and type 'Feed Instagram Lite' in the search field.

= For manual installation 1: =

1. Login to your website and go to the Plugins section of your admin panel.
1. Click the Add New button.
1. Under Install Plugins, click the Upload link.
1. Select the plugin zip file (feed-instagram-lite.x.x.x.zip) from your computer then click the Install Now button.
1. You should see a message stating that the plugin was installed successfully.
1. Click the Activate Plugin link.

= For manual installation 2: =

1. You should have access to the server where WordPress is installed. If you don't, see your system administrator.
1. Copy the plugin zip file (feed-instagram-lite.zip) up to your server and unzip it somewhere on the file system.
1. Copy the "feed-instagram-lite" folder into the /wp-content/plugins directory of your WordPress installation.
1. Login to your website and go to the Plugins section of your admin panel.
1. Look for "Instagram Feed Lite" and click Activate.

**For Mac Users**  
> * Go to your Downloads folder and locate the folder with the plugin. 
> * Right-click on the folder and select Compress. 
> * Now you have a newly created .zip file which can be installed as described here. 
> * Click "Install Now" button.  
> * Click "Activate Plugin" button for activating the plugin.

== Frequently Asked Questions ==

= How to use Instagram Feed Lite Plugin? =

There are no complicated instructions for using Instagram Feed Lite plugin because this plugin designed to make all easy. Please watch the following video and we believe that you will easily to understand it just in minutes :

[youtube https://www.youtube.com/watch?v=PN80JilwP3Y]

= Getting an error saying The access_token provided is invalid =

Please follow these steps with CAREFULLY :

* Login to instagram
* Open this link : [Access Token Manager][ac manager] and click revoke access button for GHOZY LAB LLC App Authorization

[ac manager]: https://www.instagram.com/accounts/manage_access/
            "Instagram Access Token"
			
* Back to WP admin and make sure your Instagram Pro already installed and activated. Now go to Instagram Settings. Remove  Access Token and  User ID and hit Save Changes Button
* Click RED Generate Access Token button, after generated just hit SAVE CHANGES BUTTON again to save your new token
* DONE

= How can I get support? =

* We are not able to provide anything other than community based support for Instagram Lite Plugin.

= How can I say thanks? =

* Just recommend our plugin to your friends! or
* If you really love Instagram Lite Plugin any donation would be appreciated! It helps to continue the development and support of the plugin.
But seriously, I just want to drink coffee for free, so help a developer out. You can use this link [Donate to Instagram Feed Lite][cp donate].

[cp donate]: https://plasso.co/donate@ghozylab.com
            "Donate to Instagram Feed Lite Plugin"

== Screenshots ==

1. Lite Version
2. Pro Version
3. Pro Version
4. Pro Version
5. Pro Version
5. Pro Version ( lightbox mode )

== Upgrade Notice ==

= 1.0.0.5 =

IMPORTANT! BUGS FIX, PLEASE UPDATE NOW!

== Changelog ==

= 1.0.0.5 =

* Compatibility with new Instagram API

= 1.0.0.3 =

* Fallback function if the PHP Server does not have the array_replace function
* Update : WordPress 4.8 compatibility

= 1.0.0.1 =

* Function to handle if there are no feed ID
* Update : WordPress 4.7.3 compatibility

= 1.0.0.0 =

* This is the launch version. No changes yet.