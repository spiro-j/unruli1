1.1
- Added WooCommerce 3.5.1 compatibility
- Added compatibility with PHP 7.2
- Added compatibility with Gutenberg plugin
- Updated WPBakery Page Builder to 5.5.5
- Updated Revolution Slider to 5.4.8
- Fixed uteam list pretty photo display
- Improved working hours shortcode translation

1.0.1
- Added Product list button hover effect
- Improved Banner button hover effect