<?php

if (!function_exists('curly_mkdf_include_blog_shortcodes')) {
    function curly_mkdf_include_blog_shortcodes() {
        include_once MIKADO_FRAMEWORK_MODULES_ROOT_DIR . '/blog/shortcodes/blog-list/blog-list.php';
    }

    if (curly_mkdf_core_plugin_installed()) {
        add_action('curly_core_action_include_shortcodes_file', 'curly_mkdf_include_blog_shortcodes');
    }
}

if (!function_exists('curly_mkdf_add_blog_shortcodes')) {
    function curly_mkdf_add_blog_shortcodes($shortcodes_class_name) {
        $shortcodes = array(
            'CurlyCore\CPT\Shortcodes\BlogList\BlogList',
        );

        $shortcodes_class_name = array_merge($shortcodes_class_name, $shortcodes);

        return $shortcodes_class_name;
    }

    if (curly_mkdf_core_plugin_installed()) {
        add_filter('curly_core_filter_add_vc_shortcode', 'curly_mkdf_add_blog_shortcodes');
    }
}

if (!function_exists('curly_mkdf_set_blog_list_icon_class_name_for_vc_shortcodes')) {
    /**
     * Function that set custom icon class name for blog shortcodes to set our icon for Visual Composer shortcodes panel
     */
    function curly_mkdf_set_blog_list_icon_class_name_for_vc_shortcodes($shortcodes_icon_class_array) {
        $shortcodes_icon_class_array[] = '.icon-wpb-blog-list';
        $shortcodes_icon_class_array[] = '.icon-wpb-blog-slider';

        return $shortcodes_icon_class_array;
    }

    if (curly_mkdf_core_plugin_installed()) {
        add_filter('curly_core_filter_add_vc_shortcodes_custom_icon_class', 'curly_mkdf_set_blog_list_icon_class_name_for_vc_shortcodes');
    }
}