<?php

if (!function_exists('curly_mkdf_register_button_widget')) {
    /**
     * Function that register button widget
     */
    function curly_mkdf_register_button_widget($widgets) {
        $widgets[] = 'CurlyMikadofButtonWidget';

        return $widgets;
    }

    add_filter('curly_mkdf_register_widgets', 'curly_mkdf_register_button_widget');
}