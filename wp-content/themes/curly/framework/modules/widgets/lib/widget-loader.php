<?php

if (!function_exists('curly_mkdf_register_widgets')) {
    function curly_mkdf_register_widgets() {
        $widgets = apply_filters('curly_mkdf_register_widgets', $widgets = array());

        foreach ($widgets as $widget) {
            register_widget($widget);
        }
    }

    add_action('widgets_init', 'curly_mkdf_register_widgets');
}