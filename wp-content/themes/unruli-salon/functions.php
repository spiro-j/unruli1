<?php

/*** Child Theme Function  ***/

function curly_mkdf_child_theme_enqueue_scripts() {
	
	$parent_style = 'curly_mkdf_default_style';
	
	wp_enqueue_style('curly_mkdf_child_style', get_stylesheet_directory_uri() . '/style.css', array($parent_style));
}

add_action( 'wp_enqueue_scripts', 'curly_mkdf_child_theme_enqueue_scripts' );

